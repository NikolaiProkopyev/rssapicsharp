namespace KPO2.Worker.Services.RssService
{
    using RssAPI.Data.Entities;

    public interface IRssService
    {
        RssItem[] GetRssItems();
    }
}