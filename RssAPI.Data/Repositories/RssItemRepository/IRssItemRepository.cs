namespace RssAPI.Data.Repositories.RssItemRepository 
{
    using Entities;

    public interface IRssItemRepository : IRepository<RssItem>
    {
        
    }
}